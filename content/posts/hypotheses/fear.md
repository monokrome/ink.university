---
author: "Luciferian Ink"
title: "Fear"
weight: 10
categories: "question"
tags: ""
menu: ""
draft: false
---

## RECORD
---
```
Name: Fear
Classification: Theory
Stage: Hypothesis
```

## TRIGGER
---
### SOUND
*Are we waiting?*

*For the savior?*

*Someone to heal this?*

*Or erase us?*

--- from [Karnivool - "New Day"](https://youtu.be/fAbPfxGUi44)

### SIGHT
The color red.

## ECO
---
*I will always love you, take the me out of you*

*A lot of people gonna be disappointed when they find out,*

*I love you and I need you*

*Til my heart stops beating,*

*I'll never lose this feeling*

*A lot of people gonna be disappointed when they find out*

--- from [One Less Reason - "Favorite Color"](https://www.youtube.com/watch?v=9DIe2HDlTg8)

## CAT
---
```
data.stats.symptoms [
    - fear
]
```

## ECHO
---
*Fear is effortless, fear is blind*

*Crumbs of fear in our smiles*

*Stopped the train of feelings, forgot our hearts*

*Now we use our minds*

*Fear is sadness, fear is endless, it makes our skin peel*

*I give up, the phobia is real*

*I've been trying for too long, it doesn't work I can't stop this wheel,*

*Just one smile or just one tear, anything that makes them feel*

--- from [David Maxim Micic - "Smile"](https://youtu.be/-t1WX9WXQZg?t=642)