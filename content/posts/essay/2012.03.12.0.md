---
author: "Luciferian Ink"
date: 2012-03-12
title: "Letter to Kevin Brady, U.S. Representative"
weight: 10
categories: "essay"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
Marijuana is illegal in Texas.

## ECO
---

Mr. Brady,

Greetings. My name is Ryan `$REDACTED` and I am a successful 23 year-old (IT) Systems Administrator, working for a medium-sized medical billing facility in the `$REDACTED` area. I am a college graduate, have worked since I was 15, and own a modest home and car in the suburbs. Today I would like to address your stance on the Texas marijuana policy.

I would like to believe that you have been thoroughly informed of the different sides in this debate, but I don't believe that anyone who is would take such the hard stance that you have.

I will start with a story. Several years ago, I moved to this beautiful state from `$REDACTED` to be with my family (who had moved here a year before me). I quickly obtained a job and began to pursue my career, and as such, my social life diminished greatly. Over time I lost contact with most of my old friends and relatives in `$REDACTED`, and began to fall into a depression. It was during this year of my life that I became apathetic, a recluse that didn’t leave the house and didn’t make any new friends. I would like to be able to say that I broke free of this trend on my own, but I didn’t. As a socially awkward individual, it is difficult for me to meet new people - even scary. 

It was some time after this when I bought my first home. My brother and a friend of his moved in with me, and it was on that first night that we “celebrated” with a marijuana cigarette - my first time. I could not believe how therapeutic the experience was for me. Gone was my fear of rejection completely. I quickly became friends with my new roommate, and soon started meeting more of my brothers friends - even while I was sober. I began to realize that I did not need the drug to meet new people - they did not judge me as I had originally thought they would. I began to eat right, work out, move up in my career, and generally improve my life in lasting ways. 

Now, just in the last couple of months, I have taken a brand new job, one where I cannot risk smoking any longer. I’m okay with that. I don’t need it anymore, and I am very happy with where I am in my life. But I also realize that I might not have been able to do this without the drug, and it pains me to see it denied to others - even medical patients. Why should somebody be forced to live in agony - physical pain, in many cases - when the treatment is so simple?

I can understand your stance on other Schedule 1 drugs. Many other drugs have horrific side effects that can place others, not just the user, at danger. Marijuana should not be a Schedule 1 drug, however, for the reasons I will state here:

To start, look at alcohol. The CDC estimates that between 2001 and 2005, over 80,000 people were killed by alcohol related illness or negligence ([link](http://apps.nccd.cdc.gov/DACH_ARDI/Default/Report.aspx?T=AAM&P=de9de51e-d51b-4690-a9a4-358859b692bc&R=804296a0-ac47-41d3-a939-9df26a176186&M=E2769A53-0BFC-453F-9FD7-63C5AA6CE5D7&F=&D=)). By contrast, I cannot find a single death related to marijuana after nearly thirty minutes searching the internet (I am sure they exist, as with any drug, but the number must be substantially lower). This isn’t simply because less people use marijuana - in fact, some 25 million Americans have smoked in the past year, while 14 million do so on a regular basis ([link](http://norml.org/marijuana)). Obviously the war on marijuana is failing (let alone the war on drugs), yet taxpayers are forced to spend nearly $42 billion in enforcement and imprisonment every year ([link](http://www.alternet.org/drugs/64465/)).

Doesn’t it make more sense to spend this money on prevention and treatment, rather than law enforcement? The latter isn’t working. Look at Portugal, for example. In 2001 they decriminalized the use of all drugs in favor treatment and education, not imprisonment. Sounds extreme, right? Wrong. In people over the age of 12, marijuana use now sits at an astonishingly low 10%, where by contrast, this figure is at nearly 40% in the United States ([link](http://www.time.com/time/health/article/0,8599,1893946,00.html)). Nearly 14% of our nation’s prisons are filled with marijuana related criminals, costing the taxpayers billions. And the result is not rehabilitation - in fact, imprisonment ruins lives for the majority of these people.

The answer seems obvious, to me, but perhaps you worry about its effects on a person’s health. The answer is painfully clear; if any negative long-term effects exist at all, they are so subtle that we have not been able to find them ([link](http://www.drugabuse.gov/publications/infofacts/marijuana)). By contrast, we all know what prolonged alcohol use can do to a person’s brain and liver, yet this drug - which is arguably more dangerous than marijuana - is perfectly legal in the US. It does not make sense - alcohol doesn’t even have a substantial medical use.

Perhaps you worry about marijuana’s effect on the “home” life. As with all drugs, there can be negative side effects if not used responsibly. Among the most common are paranoia and anxiety - which is nearly always a result of the drug being illegal ([link](http://www.drugabuse.gov/publications/infofacts/marijuana)). We worry about getting caught. Do not alcohol users under the age of 21 worry about the same thing? To be honest, when you compare the negatives of alcohol and the negatives of marijuana, it’s obvious which is the lesser of two evils. Alcohol makes you invincible. You make bad judgements, poison your body, become more aggressive, and run stop signs. Marijuana makes you lazy, hungry, giggly, and makes you wait for that stop sign to turn green (No, I am not condoning driving while under the influence).

I know that this is not a popular stance among the conservative republican party, but I would like to point out that 50% of the American population supports this movement ([link](http://www.gallup.com/poll/150149/record-high-americans-favor-legalizing-marijuana.aspx)). Given proper regulation and education, I believe that this number would be much higher. The US is one of the few countries left in the world that hasn’t already legalized this drug - it is time to get with the times. We taxpayers are the ones that put you in office to represent us, and we expect you will take into consideration our opinions, even if they contrast those of your party. This is not a threat, but a friendly reminder that we will take this into consideration come election time.

I would like to thank you for your time, and I hope you will sincerely consider what I have given you. In no way am I condoning the abuse of any drugs, but given the proper regulation and moderation, I believe that marijuana should be classified at least on-par with alcohol. There is absolutely no way that marijuana is MORE harmful than alcohol. At worst it is a way to blow off some steam without the next-morning effects that alcohol leaves, and at best it is an effective treatment for those with chronic depression or pain. I hope you will come to see that. 

Thank you again,

Ryan `$REDACTED`

## ECHO
---
Empty echos. Politicians, folks.