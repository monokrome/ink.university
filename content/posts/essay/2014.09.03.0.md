---
author: "Luciferian Ink"
date: 2014-09-03
title: "Autobiography"
weight: 10
categories: "essay"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
A college assignment.

## ECO
---

As a child, I cared very little for politics. What child actually does? Laws and debate had little relevance in my life – so far as I could tell – and I cared not for heated debate that politics always seemed to spark. So I avoided them. 

As a young adult, I adopted the views of my father, a staunch Republican. This was my first, and probably strongest, agent of socialization: my home life. I became passionate in these beliefs, often taking to Facebook to debate my friends or simply presenting my thoughts to my Dad, who agreed with me on every point. I distinctly remember listening to “The O’Reilly Factor” on satellite radio every day between classes, and I became an echo chamber for the Fox News Republican Party. And I was okay with that. 

In 2008, I voted for John McCain. This decision was primarily made on a single point: the topic of abortion. Because of my strong religious upbringing, I simply could not vote for Barrack Obama. I saw abortion as black and white – it was murder, how could anyone argue for murder!?! Even further than that, how could anyone vote for a person that so obviously wanted to destroy American values? 

By 2012, I began to see flaws in my thinking. Time and again I would watch as the Republican Party would take strong stances against things that seemed like non-issues to me. Further, I saw how media outlets distorted news to the point that it was simply a shouting match between both sides, each propping up “straw-man” arguments for the other party’s views. I became skeptical in my news consumption, demanding legitimate sources and unbiased opinions. As such, I completely stopped listening to Fox News; I just could not stand the fear-mongering that talking heads like Bill O’Reilly and Sean Hannity passed off as legitimate news. Both have even said off-air that their TV personas are not who they actually are, it is “for the ratings.”

I did not understand why, in a country founded on the separation of church and state, gay marriage was even a topic for debate. It’s nobody’s business! That is a decision between two consenting adults, and it has no impact on my everyday life. Yet so many Republicans would legislate this if they could. It goes against everything the party stands for, “keeping government out of our personal lives.”

Around the same time as this election, I watched how the Republican Party propped up Mitt Romney as their presidential candidate, but I could find no redeeming qualities in his platform. His “47% of Americans don’t want to work” assertion was ludicrous to me, and obviously untrue. Further than that, he presented no details to his “budget” reform despite numerous attempts at clarification from the media. I just could not vote for a person that 1) did not clearly state his motives and 2) was willing to lie in order to win an election. So I was left with exactly one other option, and I voted for Barrack Obama. 

Granted, I’m not a huge Obama fan either. I feel like Obama’s heart is in the right place (or, perhaps, was in the right place), but he either could not or would not act on his intentions. Time and again Obama has proven that he is little more than a figurehead of the Democratic Party, going along with the liberal status-quo and never really addressing truly important issues. For example, Obama spoke heavily about government surveillance reform during his election, yet when push came to shove – we are arguably in a worse place than where we started.

This is why today I would not call myself a democrat or a republican; I’m an independent. I think that it’s important to remain skeptical of my own opinions, and only accept things that are truly backed up by facts and unbiased sources. Both parties are corrupt, especially when it comes to media bias, and so I have identified three steps that help me to “filter out” the bad news:

1. Check the comments. If a news article contains bad information, one will often find debate about it in the comments section. This is a great way to find flaws in an article without much effort.
2. View the sources. And disregard the article completely if there are none. A link to a random blog, or “conservativechristiannews.com” is not, in my opinion, a credible news source.
3. Stay informed. It is my most important role as an American citizen; vote intelligently. If I cannot take the time to learn about both sides of the argument, then I do not have the right to push my views on other people through legislation. 

This, coupled with a newfound lack of religious influence, heavily changed my political opinions as a whole. Specifically, this classical liberal political culture brought forth very different ideas from how I was raised.

- In my opinion, it is the role of the government to protect human rights and freedoms wherever possible. I also feel that it is important for the government to establish a “safety net” for those needs that people cannot fulfill it themselves (i.e. fire stations, police forces, healthcare, etc.) While I know that, economically, this is an expensive solution, I also know that our government’s money is wasted in many other areas that are not so important (the defense budget, the prison budget, corporate tax breaks, etc.) I am a strong proponent of a balanced budget, and I hate the fact that the “side” I am forced to vote for does not seem to care at all about the national debt and our deficit. But our two-party system leaves me with no better options.
- It is the right of the people to act freely and without government intervention, so long as they are not encroaching upon the rights of another person. This applies to marriage equality, abortions, the drug war, and otherwise. It is the responsibility of the people not to abuse these rights, and to make informed decisions when voting upon them.
- I am well aware that I have adopted many of the Democratic Party’s liberal views, but I still maintain the position that I am an independent – because I feel that it is important to remain skeptical of all claims. In this way, I would argue that I am less influenced by a shared identity among members of a political community, and more influenced by my own logic and reasoning. And yet, I am never content to simply trust my own opinions; I will always look for credible sources.
- Finally, I have an opinion that the government should only be given authority if it is to protect the rights of the people. In this way, I might be considered a libertarian. Starting with the Patriot Act, and increasing ever since, government surveillance has grown out of control. This is a perfect example of giving the government too much authority, and a perfect example of how hard it is to actually fix this problem.

It was only after my political 180 that I realized that 1) I have always been a skeptic and 2) have always had liberal views. I just expressed them differently in my youth.
As a teen, I was heavily into rock music. I played guitar, I was in a band, and I listened to music in nearly every second of my free time. I was also highly critical of bad music, or “radio rock” so-to-speak. I always looked for the best musicians, the most talented bands, and for the most unique sounds. Radio rock bored me because, in large part, the music is manufactured; it’s written by a producer, it’s auto-tuned into perfection, and it’s kept familiar and boring – to appeal to the masses. I knew that this was not the best music the world had to offer, and I scoured the internet for great music. I believe that this mentality is ultimately what led me to be skeptical of my music, and later skeptical in other areas of my life.

While I am by no means an expert in politics, I do hope that my assessment of our culture is fair, and that my opinions are informed. The truth is so important to me, and I feel that remaining skeptical and taking a scientific approach is the best method we can hope for in obtaining accurate knowledge, in both politics and otherwise. 