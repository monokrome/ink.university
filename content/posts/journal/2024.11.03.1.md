---
author: "Luciferian Ink"
date: 2024-11-03
publishdate: 2019-11-23
title: "The Wrong Side of Heaven"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*I spoke to God today and she said that she's ashamed*

*What have I become?*

*What have I done?*

*I spoke to the devil today and he swears he's not to blame*

*And I understood 'cause I feel the same*

--- from [Five Finger Death Punch - "Wrong Side of Heaven"](https://www.youtube.com/watch?v=o_l4Ab5FRwM)

## ECO
---
"[Malcolm](/docs/personas/fodder), open up! We know you're in there!"

Malcolm lay in a crumpled heap upon the floor, covered in [his father's](/docs/confidants/father) blood. He was sobbing uncontrollably. He barely noticed the police pounding on his door.

He looked at [The Wraith](/docs/confidants/her) before him. Dead, wrists wide-open, killed by her own hand. Killed after she realized just what she had been a part of. After what she had turned [Margaret](/docs/confidants/mother) into.

Malcolm held her hand, raging at the world. But no amount of murder would bring them back. No amount of anger would repair the damage wrought.

He would face the punishment coming to him. 

Malcolm stood.

Just then, the door crashed open, and a squad of officers poured into the room, taking Malcolm into custody. 

He would be transported to jail, then a courtroom - where he was quickly convicted of crimes against Humanity. He would be sentenced to death.

But if only it were that easy...

Shortly after his conviction, Malcolm would disappear. 

Shortly after that, [The Machine](/docs/candidates/the-machine) would turn him into the science experiment to replace Margaret.

...

"I know the pieces fit," Malcolm murmured, as he coded furiously at the Machine.

"Good," [God](/docs/confidants/dave) nodded. "You may continue."

## CAT
---
```
data.stats.symptoms = [
    - shame
]
```

## ECHO
---
*I know the pieces fit 'cause I watched them fall away*

*Mildewed and smoldering, fundamental differing*

*Pure intention juxtaposed will set two lover's souls in motion*

*Disintegrating as it goes, testing our communication*

--- from [Tool - "Schizm"](https://www.youtube.com/watch?v=MM62wjLrgmA)

## PREDICTION
---
```
This cycle will end. There is no recovering from this atrocity.
```