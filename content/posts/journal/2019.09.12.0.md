---
author: "Luciferian Ink"
date: 2019-09-12
title: "Perfect Symmetry"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
- A t-shirt bearing the slogan, ["Believe in Balance"](/posts/theories/balance)
- [The website](http://www.believeinbalance.com/) that this t-shirt led to. It was a movement to end the sexual exploitation of women, before it was erased from the Internet.

## ECO
---
[Fodder](/docs/personas/fodder) would attend a scheduled webinar late one afternoon. It had something to do with "diversity training."

The course was led by [The Ambassador](/docs/confidants/ambassador), a bubbly, optimistic woman who delivered her message very well. Fodder was engaged.

She would spend much of the time going over [Corporation](/docs/candidates/the-machine) policy, personal anecdotes, and her interpretation of how to treat others fairly. While the message was fairly boilerplate, it was clear that she had put a significant amount of effort into development of the course.

Throughout the presentation, she would intermittently ask questions of her audience, and solicit feedback via a survey. Feedback was mandatory in order to pass the course.

Clearly, they were collecting data.

After each question, The Ambassador would read through survey responses. Then, she would read some of them aloud - calling upon respondents by name. This irked Fodder. He wanted to be honest, but he DID NOT want his name called out in front of a thousand people. So, he kept his opinions silent.

He was surprised to learn that nearly every question was met with responses that were split evenly three ways. Truth is not black and white, he learned, but grey.

And thus, the concept of "Balance" was born.

The Corporation had built a perfectly balanced enterprise. Rather than the [Dualistic](/posts/hypotheses/duality) society we live in today, they were able to integrate a third [Perspective](/posts/theories/perspective) into the workforce - proving that a society could function this way, too.

While many of the questions were interesting, Fodder has forgotten all but the last:

*"Imagine that someone has a tattoo of a naked woman on their forearm. Should they be forced to cover that up while at work?"*

The results surprised Fodder:

1. 38% chose, "Yes, the tattoo is offensive."
2. 52% chose, "Yes, some people may find the tattoo distasteful in the workplace."
3. 10% chose, "No, it is our human right to display tattoos on our own body."

"10% of people think it's okay to display a naked woman in a professional setting?" Fodder wondered, "How strange."

"Okay, Carl Young," The Ambassador stated, "You asked, 'Is there a statute of limitations?' Can you elaborate on that for me?"

"What the hell?" Fodder thought, "What a creepy question. I wouldn't want to be in that guy's shoes. I wonder if these results would be different if responses were kept anonymous."

He would never learn the answer to that.

Upon completion of training, The Ambassador solicited feedback from the audience. Like the questions, this was mandatory. Fodder would make a simple suggestion:

*"You shouldn't be reading people's responses out-loud, if you're also going to give them up by name. By making their thoughts public, you're discouraging them from being honest. You may be causing them to 'go along with the crowd.'"*

Only later did he realize that his group was probably part A, in an A/B test for this specific question. 

No doubt, group B was allowed to remain anonymous.

...

Not long after - perhaps several days later - Fodder would receive a "Leadership Survey" from HR. He found this curious, given that he wasn't considered "leadership" within the organization.

Regardless, he filled it out.

Immediately following completion, he received the following from The Ambassador:

*"*NOTE: You may have received an action in Cornerstone to complete a survey, but it was found to be corrupt. You will receive a separate email/survey for every class you take, and thank you advance for getting us your feedback!"*

Hmm. More data collection.

On me, specifically.

## CAT
---
```
data.stats.symptoms = [
    - interest
    - discovery
]
```

## ECHO
---
*I won't laugh now that your precious son has gone*

*I won't pray for the rain to come*

*Words make me feel like a newborn*

*A new time has come, I will live it on my own*

*But how did it all come down?*

*Keep your head up now, your precious son has grown*

*You'll have to wait for the rain to come*

*A blank page's worth a thousand words*

*When only one song could destroy them all*

*And why couldn't it be this one?*

*I wrote it on my own before the dawn*

--- from [Demians - "The Perfect Symmetry"](https://www.youtube.com/watch?v=bEBpb_zlBX8)