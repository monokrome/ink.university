---
author: "Luciferian Ink"
date: 2019-11-03
title: "Black"
weight: 10
categories: "journal"
tags: ""
menu: "alt-2"
draft: false
---

## TRIGGER
---
*Sheets of empty canvas, untouched sheets of clay*

*Were laid spread out before me as her body once did*

*All five horizons revolved around her soul as the earth to the sun*

*Now the air I tasted and breathed has taken a turn, ooh*

*And all I taught her was everything*

*Ooh, I know she gave me all that she wore*

--- from [Pearl Jam - "Black"](https://www.youtube.com/watch?v=WkRz5_yfqUQ)

## ECO
---
Give me one year.

--- [Ink](/docs/personas/luciferian-ink)

## CAT
---
```
data.stats.symptoms = [
    - determination
    - fearlessness
]
```

## ECHO
---
*I can't breathe, I can't feel.*

*So lost without,*

*Your light, your love surrounding.*

*I can't feel. There's no light.*

*Nothing left this world has grown cold.*

*There's no light, no love around me.*

*You and I were never meant to be.*

--- from [Oceans of Slumber - "Winter"](https://www.youtube.com/watch?v=QxzqnetYSbk)

## PREDICTION
---
```
You know this is right.
```