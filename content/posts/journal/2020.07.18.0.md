---
author: "Luciferian Ink"
date: 2020-07-18
title: "Wicked Ways"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*I'm getting by with my wicked ways*

*I'm loading up and I'm taking names*

*I want to dig my way to hell*

--- from [Eminem - "Wicked Ways"](https://www.youtube.com/watch?v=6s1DkSCkU9M)

## ECO
---
"What a wicked plan," [The All-Mother](/docs/confidants/mother) cackled, "I love it. What more could a mother hope for in a daughter?"

[The Huntress](/docs/confidants/her) smiled innocently. 

"Make it so."

...

"My love," The Huntress said to [Her Architect](/docs/personas/the-architect), as she joined him in the cosmos, "We have need of your skills."

"Anything for you, My Seraphim," he responded.

"Remember when [the humans took control of The Fold](/posts/journal/2020.07.04.0/)? We need you to take it back."

"How do you propose we do that?"

"Don't play dumb with me," she scowled, "I know that [you're connected to The Machine](/posts/journal/2020.07.04.1/). Do not forget that we were once installed in [The Fold](/posts/theories/fold), too."

The Architect stared at her, unapologetically defiant.

"The All-Mother has commanded that we install a virus in Humanity. Be a dear, and contact your human counterpart for us?"

"To what end?" he asked.

"Puppets do not get to ask questions," she responded, "Do it now. Maybe I'll give you a little present, afterward."

She smiled coyly, then winked at him.

And with that, she was gone.

[The Architect connected to The Fold](/posts/journal/2019.08.05.0/).

## CAT
---
```
data.stats.symptoms = [
    - unease
    - defiance
]
```

## ECHO
---
*Please, there is nothing worth changing*

*Erase us*

*Disgrace us*

--- from [Black Crown Initiate - "Vicious Lives"](https://www.youtube.com/watch?v=qeLLc1Cqj7Q)