---
author: "Luciferian Ink"
date: 2003-06-16
title: "Infidels"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*(Instrumental)*

--- from [Igorrr - "Camel Dancefloor"](https://www.youtube.com/watch?v=tZ3KObtDaGw)

## ECO
---
### The Movie
[The Tradesman](/docs/confidants/father) worked nights, in [Fodder's](/docs/personas/fodder) teen years. His job was over 1.5 hours away; so, in addition to the work, he was driving 3+ hours every day. 

He had one evening off work. Since he was home, and the only one awake, he would purchase a pay-per-view movie. An adult one.

[The Queen](/docs/confidants/mother) would discover this a couple of weeks later, while looking at the satellite T.V. bill. It broke her heart.

And she made sure the entire house knew about this indiscretion. 

### The Old Flame
In Fodder's late teenage years, The Tradesman would discover that The Queen had been in communication with an old lover, from high school. He would see her entire conversation on Facebook.

He kept quiet for a time, even though the relationship continued. At some point, he came to his kids - in tears - telling them that he thought that The Queen was going to leave him. He didn't know what to do. 

The kids let The Queen know what they thought of the situation shortly thereafter. 

The fear of losing them caused to her to break it off immediately.

And yet, she still mentions her old flame to this day.

### The Promise
After these two instances, and others, Fodder made a vow:

Should he ever find a significant other, he would never put her in this kind of situation. Not once. 

And he would break his bad habits before ever allowing her to fall for him.

## CAT
---
```
data.stats.symptoms = [
    - heartbreak
]
```

## ECHO
---
*(Instrumental)*

--- from [This Will Destroy You - "A Three-Legged Workhorse"](https://www.youtube.com/watch?v=a8E-cLMiBWM)

## PREDICTION
---
```
Neither of these events were real. They were staged.
```