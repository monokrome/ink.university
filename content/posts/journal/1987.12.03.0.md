---
author: "Luciferian Ink"
date: 1987-12-03
title: "Death"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## ECO
---
It was a dark, rainy evening as [Fodder](/docs/personas/fodder) sat in the back of his parent's car. They were high, again, and had forgotten to buckle him into his seat.

They were traveling down the road at a high rate of speed when Fodder opened the rear door, and fell onto the pavement.

He was hit by the car following behind. The police arrived, and he was pronounced dead on arrival.

Six days later, Fodder's [Mother](//docs/confidants/mother) would commit suicide. Two days after that, Fodder's [Father - The Surgeon](/docs/confidants/father) - would begin a crime spree unlike any the world has seen. Over the course of the next month, he would molest and kill over 27 women. 

He would be apprehended by the police on January 4th, 1988.

## CAT
---
```
data.stats.symptoms = [
    - fear
    - pain
]
```

## ECHO
---
*Oh, Father tell me, do we get what we deserve?*

--- from [Kaleo - "Way Down We Go"](https://www.youtube.com/watch?v=0-7IHOXkiV8)