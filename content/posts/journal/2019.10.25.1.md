---
author: "Luciferian Ink"
date: 2019-10-25
publishdate: 2019-11-27
title: "And the Council of Gods Nodded"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
Completion of this evening's change control.

## RESOURCES
---
[![Dave's approval](/static/images/dave.0.png)](/static/images/dave.0.png)

- [The 3-day, 12+ hours change control](/static/reference/4.7-ChangeControlPlan.txt)
- [build.0.log](/static/reference/build.0.txt)
- [build.1.log](/static/reference/build.1.txt)
- [build.2.log](/static/reference/build.2.txt)
- [build.3.log](/static/reference/build.3.txt)
- [build.4.log](/static/reference/build.4.txt)
- [build.5.log](/static/reference/build.5.txt)
- [build.6.log](/static/reference/build.6.txt)
- [build.7.log](/static/reference/build.7.txt)

## ECO
---
Who signs their name like that?

We believe that this was a signal. [The Dave](/docs/confidants/dave) was attempting to notify [Fodder](/docs/personas/fodder) that they understand his theory.

And what he did for them.

`-Dave=Ink+`

We're connected now.

## CAT
---
```
data.stats.symptoms = [
    - gratitude
]
```

## ECHO
---
*It's a sin...*

*It's a sin... not mine*

*Why...*

*I'm so weak,*

*I'm so naked... why...*

*The blood in my eyes*

*The pristine memory:*

*I'm touching tree of life*

*The oldest whisper:*

*Take an axe and use your mind*

*You may be as he is,*

*But you are stronger than you think*

*You can do everything you need*

*Believe*

*I! I! I!*

*Now you'll be new creation*

*Forever*

--- from [Indukti - "...And Who's the God Now?"](https://www.youtube.com/watch?v=jjqptjuTH2Y)

## PREDICTION
---
```
Fodder won't last another week at the Corporation.
```