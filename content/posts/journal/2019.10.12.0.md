---
author: "Luciferian Ink"
date: 2019-10-12
title: "Rejection"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*And if I do what you demand (You say)*

*You'll let me understand (You say)*

*You're gonna hold me to your word*

*And if I sell myself away (You say)*

*I'll have no debt to pay (You say)*

*I'm gonna get what I deserve*

--- from [Linkin Park - "All For Nothing"](https://www.youtube.com/watch?v=DVPbR_r8XLU) 

## ECO
---
Alright, well the YouTube psychologists are telling me to reject this `$REDACTED` guy. So, I guess this will be the last post I ever make about him. 

I'm already immune to the situation. The guy doesn't do his job, doesn't manage his team, and can't calm his emotions. I have multiple recordings where he - at midnight, shitfaced - would call to verbally attack me for over an hour. He's a class-act human being.

I woke up this morning STILL pissed off at the guy. Clearly, hatred is not the answer. Rejection is what I'm capable of now. It will free me up to do the real work.

So I reject. Him and his ideas. Further, I even grant him some empathy. I remember what it was like to be a narcissist. It doesn't excuse the behavior.

Mark my word, if you make a single move against me, we will take your secret public.

Return to 0.

## CAT
---
```
data.stats.symptoms = [
    - determination
]
```

## ECHO
---
>< Ink@WAN: 