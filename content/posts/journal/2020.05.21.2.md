---
author: "Luciferian Ink"
date: 2020-05-21
publishdate: 2020-05-22
title: "Alone in the World"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*I'm lost in the stars above*

*Consumed in my reflection*

*Distracted from my condemnation*

*They join me in consciousness*

--- from [Caligula's Horse - "Alone in the World"](https://www.youtube.com/watch?v=7tIfhqvAJeA)

## ECO
---
[Malcolm](/docs/personas/fodder) awoke with the strongest feeling that he had experienced something profound, but could not recall what it was. He had the vague notion that it had happened in a dream, but try as he might, he could not grasp on to any tangible concept.

He continued his day in much the same way he had for several weeks: by writing.

At some point late that evening, Malcolm found [the following tweet](https://twitter.com/peterboghossian/status/1264459502314635264) from one of his most trusted confidants, [The Philosopher](/docs/confidants/philosopher). This was the man who taught him the [socratic method](https://en.wikipedia.org/wiki/Socratic_method), through his book, "A Manual for Creating Atheists."

Of course, the man's [name is Peter](/docs/personas/luciferian-ink). [The tweet asked the question](https://twitter.com/peterboghossian/status/1264459502314635264):

*What’s a great argument to convince someone that what people believe matters?*

Quite narcissistically, Malcolm simply posted a link to his research.

Whereas every other reply had received a response from The Philosopher, Malcolm's was immediately [followed with a group reply](https://twitter.com/peterboghossian/status/1264470939472805888):

*That was 45 minutes. Thanks for making me think. It’s past 1:00am, need to get some sleep.*

Within 24 hours, the Philosopher had completely scrubbed Twitter of all tweets, dating all the way back to 2015.

Another [empty echo](/posts/journal/2024.11.03.0/). It's always an empty echo.

This was their game. This was Malcolm's [Solitary Confinement](/docs/scenes/solitary/). 

And somehow, he knew that release was imminent.

## CAT
---
```
data.stats.symptoms = [
    - loneliness
```

## ECHO
---
*His figure benighted*

*By revelation on his face*

*Beyond all refutation*

*And I understand his intentions*

*But don't say a word*

--- from [Caligula's Horse - "Equally Flawed"](https://www.youtube.com/watch?v=2iKQWYnKa7s)