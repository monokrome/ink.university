---
author: "Luciferian Ink"
date: 2011-12-06
title: "The Invisible Man"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
A memory.

## ECO
---
### The First Love
In 12th grade, a girl named Nancy asked [Fodder](/docs/personas/fodder) out to prom. 

"What the hell," Fodder thought, quickly accepting. "It's her, or nobody, I suppose."

The two would date for 9 months. Fodder would realize early on that she was not the one for him, but he was having fun - and so was she - so he let it continue until the start of college.

Eventually though, he would end it. 13 years later, Fodder realized why he never truly loved her:

She was independent. She did her own thing. She never reflected Fodder's ideas back at him. She never wanted to do the activities he wanted to do. They were both from completely different worlds; Fodder, a city boy. And her, a country girl.

To her, Fodder was a distraction. He was already the 3rd or 4th boyfriend.

He was the Invisible Man.

### The Holy Woman
After graduation from high school, [Mother](/docs/confidants/mother) urged Fodder to date Katelyn - the local holy woman. She, too, had just graduated, and was planning to go away for college. She had just broken up with her boyfriend of 3 years.

Trusting Mother, Fodder would go on to pursue. The two would go on one date - which would end abruptly when Fodder parroted something that his mother had once said:

"So what do you think about gay people?" Katelyn would ask.

"I think we should strap them to the back of a truck, and drag them down the street," Fodder would reply - immediately taken-aback by what he had just said. He was horrified.

The date would end shortly thereafter. Katelyn would ask Fodder not to contact her again.

```
It's possible that Mother actually said, "People used to strap them to the back of a truck," and Fodder misheard or is misremembering her words. Either way, Fodder attributes this bad memory to Mother's words.
```

### The Harlot
At Fodder's first "real" job, he would become an IT Manager for a large healthcare corporation.

Within the first year or two, a new girl would join: Jane. Fodder though Jane was really cool; not the least of which was because she made a point to visit him every day. She'd come into his office, just to "shoot the shit," as she used to say. She would spend much of the time complaining about her ex-boyfriend. Yet, somehow, Fodder started to believe that she was into him.

He would spend a few months working up the courage. Then, he would ask her on a date.

"Oh... um, yeah... that might be fun," she would reply, noncommittally. 

Completely missing the cues, Fodder would go on to brag to his co-workers later that day. Soon, the whole office knew about it - even HR.

Jane would send Fodder a message on Facebook later that day:

"I don't think we should hang out anymore. Do you have any idea how much you embarrassed me?"

This cut Fodder to his soul. In his agony, he would become [The Architect](/docs/personas/the-architect).

And he would cut her deep.

## CAT
---
```
data.stats.symptoms = [
    - confusion
    - heartbreak
    - misunderstanding
    - horror
]
```

## ECHO
---
*I know they buried her body with others*

*Her sister and mother and 500 families*

*And will she remember me 50 years later*

*I wished i could save her in some sort of time machine*

*Know all your enemies*

*We know who our enemies are*

--- from [Neutral Milk Hotel - "Oh Comely"](https://www.youtube.com/watch?v=Z-fjyEIgWik)