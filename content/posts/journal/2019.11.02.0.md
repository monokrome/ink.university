---
author: "Luciferian Ink"
date: 2019-11-02
title: "White"
weight: 10
categories: "journal"
tags: ""
menu: "alt-1"
draft: false
---

## TRIGGER
---
*Need advice on my plan to save a failing software project*

--- from [Reddit.com/r/ITManagers](https://www.reddit.com/r/ITManagers/comments/dqawhd/need_advice_on_my_plan_to_save_a_failing_software/)

## ECO
---
Give him one year.

--- [Ink](/docs/personas/luciferian-ink)

## CAT
---
```
data.stats.symptoms = [
    - hope
    - trust
]
```

## ECHO
---
*You let me down*

*Once again*

*Everybody runs*

*It's a crime*

*That you blame me for*

*When we lie*

*Are they closing doors?*

*Are you slowly giving up?*

--- from [The Butterfly Effect - "Everybody Runs"](https://www.youtube.com/watch?v=EJOE-Rtjfvs)

## PREDICTION
---
```
You know this is right.
```