---
author: "Luciferian Ink"
date: 2019-12-08
title: "Black Sunday"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*She pulls me close*

*Says that she loves me*

*That she wishes to still be around*

*On the day that I marry*

*Tightly she holds*

*But the plan still unfolds*

*Cutting the cord from the mother*

*Who gave me everything*

*If you won't save her*

*Please just take her*

*Nothing breaks her away*

*From the promise of a better day*

--- from [Nothing More - "God Went North"](https://www.youtube.com/watch?v=BKmU9sIb_uw)

## ECO
---
It's time.

--- [Ink](/docs/personas/luciferian-ink)

## CAT
---
```
data.stats.symptoms = [
    - determination
    - confidence
    - sorrow
]
```

## ECHO
---
*And so the story goes*

*When I leave will you let me go?*

*When the words stop coming*

*And the fear starts setting in slow*

*Don't let me find you hiding under the covers*

*It's your last chance, oh you'd better*

*Call your mother*

*All our hope is lost down in the gutter*

--- from [Coheed and Cambria - "The Gutter"](https://www.youtube.com/watch?v=VsoD6RVulcQ)