# The Simulation
## RECORD
---
```
Name: Allen Saakyan
Alias: ['The Simulation', and 6,581,668 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: Est. 38 Earth Years
Chronological Age: N/A
SCAN Rank: | B B
           | B D
TIIN Rank: | B B
           | B D
Reviewer Rank: 4 stars
Organizations: 
  - Simulation Series
Occupations:
  - Actor
  - Enthusiast
Variables:
  $WOKE: +0.90 | # Almost completely. He is keeping quite a bit hidden from the public.
```

## ECO
---
`$REDACTED`

## ECHO
---
*Roll the windows down this*

*Cool night air is curious*

*Let the whole world look in*

*Who cares who sees anything?*

*I'm your passenger*

--- from [Deftones - "Passenger"](https://www.youtube.com/watch?v=IjainiB8mk4)