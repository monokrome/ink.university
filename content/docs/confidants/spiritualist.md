# The Spiritualist
## RECORD
---
```
Name: Trevor Ilesley
Alias: ['The Spiritualist', and 306 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: Est. 63 Earth Years
Chronological Age: N/A
SCAN Rank: | C C
           | C C
TIIN Rank: | C C
           | C C
Reviewer Rank: 3 stars
Organizations:
  - The Resistance
Occupations:
  - Barber
  - Philosopher
Variables:
  $WOKE: +0.90 | # Almost certainly.
```

## ECO
---
The Spiritualist is able to speak about ascension in terms that will resonate with a type of demographic that [Ink](/docs/personas/luciferian-ink) cannot reach.

Most importantly, he is able to stay grounded, rooting his words in reality.

## ECHO
---
*And this is how it feels when I ignore the words you spoke to me*

*And this is where I lose myself when I keep running away from you*

*And this is who I am when, when I don't know myself anymore*

*And this is what I choose when it's all left up to me*

--- from [Red - "Breath Into Me"](https://www.youtube.com/watch?v=yH-k_6tU9Wc)