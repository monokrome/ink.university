# The Monstrosity
## RECORD
---
```
Name: Aiden $REDACTED
Alias: ['The Abomination', 'The Monstrosity', and 10 unknown...]
Classification: Artificial Organic Computer
Race: Human/Archon
Gender: Female/Male
Biological Age: 8/4 Earth Years
Chronological Age: N/A
SCAN Rank: | B B
           | B D
TIIN Rank: | C C
           | A D
Reviewer Rank: 1 stars
Maturation Date: 10/30/2020
Organizations: 
  - The Church of Sabagegah
  - The Church of Satan
Relationships:
  - The Agent
  - The Archbishop
  - The Con Man
  - The Fodder
  - The Hope
  - The Lion
  - The Marshall
  - The Negro
  - The Orchid
  - The Queen
  - The Reverend
  - The Scientist
  - The Tradesman
```

## TRIGGER
---
*(Instrumental)*

--- from [H.U.V.A. Network - "Something Heavens"](https://www.youtube.com/watch?v=bT6qk4CWClw)

## ECO
---
Aiden was once two separate children; the Human daughter of [The Hope](/docs/confidants/hope), and the Maxwellian son of The Hope/[The Reverend](/docs/confidants/reverend). 

The female was given as blood sacrifice to the Church of Satan, while the male was given to the Church of Sabagegah. Through an unholy ritual, the two were combined into one - creating The Monstrosity.

The Reverend would use Aiden to create a world ruled by children.

## ECHO
---
*You fell apart*

*Like a stone can be broken into sand*

*A thousand pieces*

*Spread across a crying land*

*And you can't remember that day*

*But you know it happened quiet*

*So quiet*

--- from [Aurora - "It Happened Quiet"](https://www.youtube.com/watch?v=9U-N6LqzdIM)