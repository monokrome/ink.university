# The Concierge
## RECORD
---
```
Name: Matt $REDACTED
Alias: ['The Concierge', and 6 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: 30 Earth Years
Chronological Age: N/A
SCAN Rank: | B D
           | B D
TIIN Rank: | C B
           | D D
Reviewer Rank: 1 stars
Location: The Oceans
Organizations:
  - Articulate Design
Occupations: 
  - Concierge
  - Cruise ship owner
Variables:
  $EMPATHY: +0.50 | # Uses much of his money to help others.
  $WOKE:    +0.20 | # Sort of. He is being kept in the dark for now.
```

## ECO
---
The Concierge was put in-charge of the cruise ship that will serve as the world's first travelling [Lonely Town](/docs/scenes/lonely-towns).

He is responsible for all amenities on the ship, as well as daily operations. He has reserved the two premiere suites - the Black and Golden Pearl - for the king and queen of this town.

## PREDICTION
---
```
He will be the first to speak openly with Fodder.
```