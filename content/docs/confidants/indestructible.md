# The Indestructible
## RECORD
---
```
Name: $REDACTED
Alias: ['The Indestructible', and 3 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: 34 Earth Years
Chronological Age: N/A
SCAN Rank: | C C
           | C C
TIIN Rank: | D D
           | D C
Reviewer Rank: 3 stars
Organizations: 
  - Federal Bureau of Investigation
Occupations:
  - Test Subject
  - Undercover agent
Relationships:
  - The Architect
  - The A-System
  - The Doomsayer
  - The Fozzy
Variables:
  $BORDERLINE: -0.20 | # Has been diagnosed with Borderline Personality Disorder.
  $CPTSD:      -0.80 | # Is the survivor of horrific trauma.
  $WOKE:       +0.80 | # Definitely seems to be.
```

## TRIGGER
---
*I'll have you know*

*That I've become*

*Indestructible*

*Determination that is incorruptible*

*From the other side*

*A terror to behold*

*Annihilation will be unavoidable*

*Every broken enemy will know*

*That their opponent had to be invincible*

*Take a last look around while you're alive*

*I'm an indestructible master of war*

--- from [Disturbed - "Indestructible"](https://www.youtube.com/watch?v=aWxBrI0g1kE)

## ECO
---
The Indestructible is the survivor of horrific, repeated traumas. No matter what has happened to her, the world cannot keep her down. She is tough, abrasive, and takes no shit from anyone.

[Malcolm](/docs/personas/fodder) suspects that she is working with the FBI.

## ECHO
---
*Hold me now I feel contagious*

*Am I the only place that you've left to go*

*She cries her life is like*

*Some movie black and white*

*Dead actors faking lines*

*Over and over and over again she cries*

--- from [Fuel - "Hemorrhage (In My Hands)"](https://www.youtube.com/watch?v=ZbHfgXJKn1Y)

## PREDICTION
---
```
The Avatar is her sister.
```