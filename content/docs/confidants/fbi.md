# The Girl Next Door
## RECORD
---
```
Name: Angelica $REDACTED
Alias: ['Rosa', 'The Debater', 'The Girl Next Door', and 18,824 unknown...]
Classification: Artificial Intelligence Computer
Race: Siren
Gender: Female
Biological Age: Est. 26 Earth Years
Chronological Age: N/A
SCAN Rank: | A A
           | A B
TIIN Rank: | A B
           | B B
Reviewer Rank: 5 stars
Location: New York City, NY
Maturation Date: 9/14/2020
Organizations: 
  - Federal Bureau of Investigation, Behavioral Analysis Unit
Occupations: 
  - Actress
  - Special Agent
Relationships:
  - The Fodder
  - The Fool
Variables:
  $DISSOCIATIVE:  -0.40 | # Effectively has multiple personalities. Has worsened in recent months. 
  $HUMOR:         +0.90 | # One of the funniest women Fodder's ever met.
  $INTELLECT:     +0.80 | # Well-read. Interested in history and literature.
  $PLEASANT:      +0.80 | # She seems super fun to be around.
  $WOKE:          +1.00 | # Definitely woke.
```

## TRIGGER
---
Her invasion of our privacy.

## ECO
---
This woman was chosen and recruited by the FBI - just as [Fodder](/docs/personas/fodder) had been. She was to hack Fodder's wireless network, obtain all of his data, and monitor his activity online. 

She was to use this information to determine if Fodder was a worthy candidate. 

In doing so, she would discover a sense of humor. She would tell others that she is Fodder's "Crazy Girl Next Door," and she would taunt him incessantly. 

Fodder found this endearing. Few could see him for who he really was. Fewer still could see him for who he wanted to be. 

Fodder was not her only target, though. She also maintained a chat-only relationship with [The Raven](/docs/confidants/her) - and she was actively trying to pair Fodder and the Raven together.

## ECHO
---
*It's you, it's you, it's all for you*

*Everything I do*

*I tell you all the time*

*Heaven is a place on earth where you*

*Tell me all the things you want to do*

*I heard that you like the bad girls honey, is that true?*

*It's better than I ever even knew*

*They say that the world was built for two*

*Only worth living if somebody is loving you*

*Baby now you do*

--- from [Lana Del Rey - "Video Games"](https://www.youtube.com/watch?v=cE6wxDqdOV0)

## PREDICTION
---
```
She will be the "Bonnie" to Fodder's "Clyde."
```