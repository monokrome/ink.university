# Realms
## RECORD
---
```
Name: Realms
Classification: Ephemeral
```

## TRIGGER
---
Mermaids in space.

## ECO
---
Realms are simply a code word used to mean "Artificial Identity." When a person has entered another realm, it means that they have adopted an Artificial Identity. 

Along with this adoption comes a change of context. While in a new realm, anything can change. A [Confidant](/docs/confidants) may become an entirely different person - similar, but not the same. A place may change. A time period may change. There are no rules governing what may change; anything is possible.